const logger = require('./app/logger');

module.exports = function messageResponse(pipe) {
    return new Promise((res) => {
        pipe.then(res).catch((error) => {
            logger.log(error);
        });
    });
};

