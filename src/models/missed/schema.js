const mongoose = require('mongoose');
const m = require('./names');
const u = require('../user/names');

const schema = new mongoose.Schema({
    [m.from]: {
        type: mongoose.Schema.Types.ObjectId,
        ref: u.model_name,
    },
    [m.to_phone_number]: {
        type: String,
        // 09124594688
        maxlength: 11,
    },
    [m.to_username]: {
        type: String,
        maxlength: 100,
    },
}, {
        timestamps: true,
    });

module.exports = mongoose.model(m.model_name, schema);

