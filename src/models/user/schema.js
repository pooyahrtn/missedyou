const mongoose = require('mongoose');
const u = require('./names');


const schema = new mongoose.Schema({
    [u.user_id]: {
        type: Number,
        required: true,
        index: true,
    },
    [u.username]: {
        type: String,
        maxlength: 200,
        index: true,
    },
    [u.lusername]: {
        type: String,
        maxlength: 200,
        index: true,
    },
    [u.first_name]: {
        type: String,
    },
    [u.last_name]: {
        type: String,
    },
});

module.exports = mongoose.model(u.model_name, schema);

