module.exports = {
    parse_mode: 'Markdown',
    reply_markup: JSON.stringify({
        keyboard: [
            [{ text: 'کیا دلتنگمن؟' }],
        ],
        one_time_keyboard: true,
    }),
};
