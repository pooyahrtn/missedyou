const { User, u } = require('../models').user;

module.exports = async function getOrCreateUser({
    id, username, first_name, last_name,
}) {
    let user = await User.findOne({ [u.user_id]: id });
    if (!user) {
        user = await User.create({
            [u.user_id]: id,
            [u.username]: username,
            [u.first_name]: first_name,
            [u.last_name]: last_name,
            [u.lusername]: username.toLowerCase(),
        });
    }
    return user;
};
