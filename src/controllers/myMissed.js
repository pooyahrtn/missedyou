const { m, Missed } = require('../models/missed');

module.exports = function myMissed(username) {
    return Missed.find({
        [m.to_username]: username,
    }).populate('from');
};
