const { u, User } = require('../models/user');
const keyboardOptions = require('../responseoptions');

module.exports = async function sendMissed(bot, from, { username }) {
    const user = await User.findOne({
        [u.lusername]: username,
    });
    if (user) {
        bot.sendMessage(user.user_id, `${from.username}\n دلش برات تنگ شده`, keyboardOptions);
        bot.sendMessage(from.user_id, 'ارسال شد!', keyboardOptions);
    } else {
        bot.sendMessage(from.user_id, `${username.replace('_', '')} \n تو بات عضو نشده. \n
        هر وقت که عضو شد، میفهمه دلتنگشی :)) wtf
        `, keyboardOptions);
    }
};
