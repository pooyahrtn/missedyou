const { m, Missed } = require('../models/missed');
const sendMissed = require('./sendMissed');

module.exports = async function addMissedPhone(bot, user, phone_number, username) {
    const existingItem = await Missed.findOne({
        [m.from]: user._id,
        [m.to_username]: username,
    });
    if (!existingItem) {
        await Missed.create({
            [m.from]: user._id,
            [m.to_phone_number]: phone_number,
            [m.to_username]: username,
        });
        await sendMissed(bot, user, { username });
    } else {
        bot.sendMessage(user.user_id, 'یبار گفتی');
    }
};
