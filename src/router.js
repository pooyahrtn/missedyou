const bot = require('./app/bot');
const getOrCreateUser = require('./controllers/getOrCreateUser');
const addMissedPhoneNumber = require('./controllers/addMissedPhonNumber');
const allPeople = require('./controllers/allPeople');
const myMissed = require('./controllers/myMissed');
const messages = require('./messages');
const messageResponse = require('./messageResponse');
const keyboardOptions = require('./responseoptions');

// const pipe = function buildAsyncpipe(...steps) {
//     return function asyncpipe(arg) {
//         return steps.reduce((result, nextStep) => result.then(nextStep), Promise.resolve(arg));
//     };
// };

// function isPhoneNumber(text) {
//     const trimmed = text.replace(/\s*/g, '');
//     return trimmed.match(/\d{11}/g);
// }

function defaultMessage(user, msg) {
    const { text } = msg;
    const trimmed = text.replace(/\s*/g, '').toLowerCase().replace('@', '');
    // if (isPhoneNumber(trimmed)) {
    //     messageResponse(addMissedPhoneNumber(bot, user, trimmed))
    //         .then(() => {
    //             console.log('ok');
    //         });
    // } else {
    messageResponse(addMissedPhoneNumber(bot, user, undefined, trimmed))
        .then(() => {
            console.log('okokokok');
        });
    // }
}

bot.on('message', (msg) => {
    getOrCreateUser(msg.from).then((user) => {
        switch (msg.text) {
            case '/start':
                bot.sendMessage(msg.chat.id, messages.welcome, keyboardOptions);
                break;
            case '/all':
                if (msg.from.username === 'Pooyahrtn') {
                    allPeople().then((res) => {
                        bot.sendMessage(msg.chat.id, `${res.map(item => `@${item.username.replace('_', '\_')} \n`)}`);
                    });
                }
                break;
            case 'کیا دلتنگمن؟':
                myMissed(msg.from.username.toLowerCase()).then((res) => {
                    if (res.length === 0) {
                        bot.sendMessage(msg.chat.id, 'هیچکی :))');
                    } else {
                        bot.sendMessage(msg.chat.id, `${res.map(item => `@${item.from.username.replace('_', '\_')}`)}`);
                    }
                });
                break;
            default:
                defaultMessage(user, msg);
        }
    });
});

