const vars = require('./vars');
const bot = require('./bot');
const mongoose = require('./mongo').connect();

exports.vars = vars;
exports.bot = bot;
exports.mongoose = mongoose;
