const vars = require('./vars');
const TelegramBot = require('node-telegram-bot-api');
const token = vars.token;
const bot = new TelegramBot(token, { polling: true });

module.exports = bot;
